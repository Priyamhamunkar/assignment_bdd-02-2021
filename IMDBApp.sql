CREATE DATABASE IMDB

USE IMDB

SET DATEFORMAT MDY
-------------------------------------------
---Table Actors------
CREATE TABLE Actors (
	Id INT PRIMARY KEY IDENTITY
	,Name VARCHAR(100)
	,Bio VARCHAR(100)
	,DOB DATE
	,Gender VARCHAR(100)
	)

INSERT INTO Actors (
	Name
	,Bio
	,DOB
	,Gender
	)
VALUES (
	'Mila Kunis'
	,'Actor1'
	,'11/14/1986'
	,'Female'
	)
	,(
	'Robert DeNiro'
	,'Actor2'
	,'07/10/1957'
	,'Male'
	)
	,(
	'George Michael'
	,'Actor3'
	,'11/23/1978'
	,'Male'
	)
	,(
	'Mike Scott'
	,'Actor4'
	,'08/06/1969'
	,'Male'
	)
	,(
	'Pam Halpert'
	,'Actor5'
	,'09/26/1996'
	,'Female'
	)
	,(
	'Dame Judi Dench'
	,'Actor6'
	,'04/05/1947'
	,'Female'
	);

CREATE PROCEDURE [dbo].[AddNewActorDetails] (
	@Name VARCHAR(50)
	,@Bio VARCHAR(50)
	,@DOB VARCHAR(50)
	,@Gender VARCHAR(50)
	)
AS
BEGIN
	INSERT INTO Actors
	VALUES (
	    @Name
		,@Bio
		,@DOB
		,@Gender
		)
END

CREATE PROCEDURE [dbo].[UpdateActorDetails] (
	@Id INT
	,@Name VARCHAR(50)
	,@Bio VARCHAR(50)
	,@DOB VARCHAR(50)
	,@Gender VARCHAR(50)
	)
AS
BEGIN
	UPDATE Actors
	SET Name = @Name
		,Bio = @Bio
		,DOB = @DOB
		,Gender = @Gender
	WHERE Id = @Id
END

CREATE PROCEDURE [dbo].[DeleteActorById] (@Id INT)
AS
BEGIN
	DELETE
	FROM Actors
	WHERE Id = @Id
END

-------------------------------------------------------------------------------
----Table Producers----
CREATE TABLE Producers (
	Id INT PRIMARY KEY IDENTITY
	,Name VARCHAR(100)
	,Bio VARCHAR(100)
	,DOB DATE
	,Gender VARCHAR(100)
	)

INSERT INTO Producers (
	Name
	,Bio
	,DOB
	,Gender
	)
VALUES (
	'Ravi'
	,'Producer1'
	,'11/24/1986'
	,'Female'
	)
	,(
	'Arun'
	,'Producer2'
	,'06/19/1957'
	,'Male'
	)
	,(
	'Tejal'
	,'Producer3'
	,'12/2/1978'
	,'Male'
	)
	,(
	'Akash'
	,'Producer4'
	,'03/07/1969'
	,'Female'
	);

CREATE PROCEDURE [dbo].[AddNewProducerDetails] (
	@Name VARCHAR(50)
	,@Bio VARCHAR(50)
	,@DOB VARCHAR(50)
	,@Gender VARCHAR(50)
	)
AS
BEGIN
	INSERT INTO Producers
	VALUES (
		@Name
		,@Bio
		,@DOB
		,@Gender
		)
END

CREATE PROCEDURE [dbo].[UpdateProducerDetails] (
	@Id INT
	,@Name VARCHAR(50)
	,@Bio VARCHAR(50)
	,@DOB VARCHAR(50)
	,@Gender VARCHAR(50)
	)
AS
BEGIN
	UPDATE Producers
	SET Name = @Name
		,Bio = @Bio
		,DOB = @DOB
		,Gender = @Gender
	WHERE Id = @Id
END

CREATE PROCEDURE [dbo].[DeleteProducerById] (@Id INT)
AS
BEGIN
	DELETE
	FROM Producers
	WHERE Id = @Id
END

-------------------------------------------------------------------
----Table Genre----
CREATE TABLE Genre (
	Id INT PRIMARY KEY IDENTITY
	,Name VARCHAR(100)
	)

INSERT INTO Genre (Name)
VALUES ('Humour')
	,('Action')
	,('Romantic')

CREATE PROCEDURE [dbo].[AddNewGenreDetails] (@Name VARCHAR(50))
AS
BEGIN
	INSERT INTO Genre
	VALUES (@Name)
END

CREATE PROCEDURE [dbo].[UpdateGenreDetails] (
	@Id INT
	,@Name VARCHAR(50)
	)
AS
BEGIN
	UPDATE Genre
	SET Name = @Name
	WHERE Id = @Id
END

CREATE PROCEDURE [dbo].[DeleteGenreById] (@Id INT)
AS
BEGIN
	DELETE
	FROM Genre
	WHERE Id = @Id
END

--------------------------------------------------------
----Table MovieActors----
CREATE TABLE MovieActors (
	Id INT PRIMARY KEY IDENTITY
	,MovieId INT FOREIGN KEY REFERENCES Movies(Id)
	,ActorId INT FOREIGN KEY REFERENCES Actors(Id)
	,
	)
	
----------------------------------------------
----Table MovieGenres----
CREATE TABLE MovieGenres (
	Id INT PRIMARY KEY IDENTITY
	,MovieId INT FOREIGN KEY REFERENCES Movies(Id)
	,GenreId INT FOREIGN KEY REFERENCES Genre(Id)
	,
	)

--------------------------------------------------------------
----Table Movies----
CREATE TABLE Movies (
	Id INT PRIMARY KEY IDENTITY
	,Name VARCHAR(100)
	,YearOfRelease VARCHAR(100)
	,Plot VARCHAR(100)
	,ProducerId INT
	,Poster VARCHAR(100)
	)

CREATE PROCEDURE [dbo].[AddNewMovieDetails] (
	@Name VARCHAR(50)
	,@YearOfRelease VARCHAR(100)
	,@Plot VARCHAR(100)
	,@ProducerId INT
	,@Poster VARCHAR(500)
	,@ActorIds VARCHAR(100)
	,@GenreIds VARCHAR(100)
	)
AS
BEGIN
	DECLARE @MovieId INT

	INSERT INTO Movies (
		Name
		,YearOfRelease
		,Plot
		,ProducerId
		,Poster
		)
	VALUES (
		@Name
		,@YearOfRelease
		,@Plot
		,@ProducerId
		,@Poster
		)

	SET @MovieId = SCOPE_IDENTITY()

	INSERT INTO MovieActors (
		MovieId
		,ActorId
		)
	SELECT @MovieId [MovieId]
		,[value] [ActorId]
	FROM string_split(@ActorIds, ',')

	INSERT INTO MovieGenres (
		MovieId
		,GenreId
		)
	SELECT @MovieId [MovieId]
		,[value] [GenreId]
	FROM string_split(@GenreIds, ',')
END

--select * from string_split('1,2,3', ',');

CREATE PROCEDURE [dbo].[UpdateMovieDetails] (
	@Id INT
	,@Name VARCHAR(50)
	,@YearOfRelease VARCHAR(100)
	,@Plot VARCHAR(100)
	,@ProducerId INT
	,@Poster VARCHAR(500)
	,@ActorIds VARCHAR(100)
	,@GenreIds VARCHAR(100)
	)
AS
BEGIN

	UPDATE Movies
	SET Name = @Name
		,YearOfRelease = @YearOfRelease
		,Plot = @Plot
		,ProducerId = @ProducerId
		,Poster = @Poster
	WHERE Id = @Id

	DELETE MovieActors
	WHERE MovieId = @Id

	DELETE MovieGenres
	WHERE MovieId = @Id

	INSERT INTO MovieActors (
		MovieId
		,ActorId
		)
	SELECT @Id [MovieId]
		,[value] [ActorId]
	FROM string_split(@ActorIds, ',')

	INSERT INTO MovieGenres (
		MovieId
		,GenreId
		)
	SELECT @Id [MovieId]
		,[value] [GenreId]
	FROM string_split(@GenreIds, ',')
END

CREATE PROCEDURE [dbo].[DeleteMovieDetails] (@MovieId INT)
AS
BEGIN
	DELETE
	FROM MovieActors
	WHERE MovieId = @MovieId

	DELETE
	FROM MovieGenres
	WHERE MovieId = @MovieId

	DELETE
	FROM Movies
	WHERE Id = @MovieId
END

-------------------------------------------------------------------


SELECT *
FROM Actors

SELECT *
FROM Producers

SELECT *
FROM Genre

SELECT *
FROM Movies

SELECT *
FROM MovieGenres

SELECT *
FROM MovieActors

DROP TABLE Actors

DROP TABLE Producers

DROP TABLE Genre

DROP TABLE Movies

DROP TABLE MovieActors

DROP TABLE MovieGenres

---------------------------------------------------------------------




