﻿using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SessionDemoApp.Models;
using SessionDemoApp.Repository;
using SessionDemoApp.ResponseModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SessionDemo.Test.MockResources
{
    public class MovieMock
    {
        public static readonly Mock<IMovieRepository> MovieRepoMock = new Mock<IMovieRepository>();
        public static readonly Mock<IActorRepository> ActorRepoMock = new Mock<IActorRepository>();
        public static readonly Mock<IGenreRepository> GenreRepoMock = new Mock<IGenreRepository>();
        public static readonly Mock<IProducerRepository> ProducerRepoMock = new Mock<IProducerRepository>();


        public static void MockGetAll()
        {
            var movies = JsonConvert.DeserializeObject<List<Movie>>(
                           File.ReadAllText(Environment.CurrentDirectory +
                                            "\\ResponseData\\Moviedata.json"));
            var actors = JsonConvert.DeserializeObject<List<Actor>>(
                           File.ReadAllText(Environment.CurrentDirectory +
                                            "\\ResponseData\\ActorData.json"));
            var genres = JsonConvert.DeserializeObject<List<Genre>>(
                           File.ReadAllText(Environment.CurrentDirectory +
                                            "\\ResponseData\\GenreData.json"));
            var movieactors = JsonConvert.DeserializeObject<List<JObject>>(
                           File.ReadAllText(Environment.CurrentDirectory +
                                            "\\ResponseData\\MovieActor.json"));
            var moviegenres = JsonConvert.DeserializeObject<List<JObject>>(
                           File.ReadAllText(Environment.CurrentDirectory +
                                            "\\ResponseData\\MovieGenre.json"));
            var producers = JsonConvert.DeserializeObject<List<Producer>>(
                           File.ReadAllText(Environment.CurrentDirectory +
                                            "\\ResponseData\\ProducerData.json"));

            MovieRepoMock.Setup(repo => repo.ListAllMovies()).Returns(() => movies);
          
            ActorRepoMock.Setup(repo => repo.GetActorsByMovieId(It.IsAny<int>())).Returns((int id) =>
            {
                var ActorIds = movieactors.Where(m => (int)(m["MovieId"]) == id).Select(a => (int)a["ActorId"]);
                return  actors.Where(a => ActorIds.Contains(a.Id));
            });

            GenreRepoMock.Setup(repo => repo.GetGenresByMovieId(It.IsAny<int>())).Returns((int id) =>
            {
                var GenreIds = moviegenres.Where(m => (int)(m["MovieId"]) == id).Select(g => (int)g["GenreId"]); //.Cast<int>();
                return genres.Where(g => GenreIds.Contains(g.Id));
            });

            ProducerRepoMock.Setup(repo => repo.GetById(It.IsAny<int>())).Returns((int id) => producers.Single(m => m.Id == id));
        }

        public static void MockGetMovieById()
        {
            var movies = JsonConvert.DeserializeObject<List<Movie>>(
                           File.ReadAllText(Environment.CurrentDirectory +
                                            "\\ResponseData\\Moviedata.json"));
            var actors = JsonConvert.DeserializeObject<List<Actor>>(
                           File.ReadAllText(Environment.CurrentDirectory +
                                            "\\ResponseData\\ActorData.json"));
            var genres = JsonConvert.DeserializeObject<List<Genre>>(
                           File.ReadAllText(Environment.CurrentDirectory +
                                            "\\ResponseData\\GenreData.json"));
            var movieactors = JsonConvert.DeserializeObject<List<JObject>>(
                           File.ReadAllText(Environment.CurrentDirectory +
                                            "\\ResponseData\\MovieActor.json"));
            var moviegenres = JsonConvert.DeserializeObject<List<JObject>>(
                           File.ReadAllText(Environment.CurrentDirectory +
                                            "\\ResponseData\\MovieGenre.json"));

            MovieRepoMock.Setup(repo => repo.GetMovieById(It.IsAny<int>())).Returns((int id)=>movies.Single(m=>m.Id==id));

            ActorRepoMock.Setup(repo => repo.GetActorsByMovieId(It.IsAny<int>())).Returns((int id) =>
            {
                var ActorIds = movieactors.Where(m => (int)(m["MovieId"]) == id).Select(a => (int)a["ActorId"]);
                return actors.Where(a => ActorIds.Contains(a.Id));
            });

            GenreRepoMock.Setup(repo => repo.GetGenresByMovieId(It.IsAny<int>())).Returns((int id) =>
            {
                var GenreIds = moviegenres.Where(m => (int)(m["MovieId"]) == id).Select(g => (int)g["GenreId"]).Cast<int>();
                return genres.Where(g => GenreIds.Contains(g.Id));
            });
        }

        public static void MockAddMovie()
        {
            MovieRepoMock.Setup(repo => repo.AddMovie(It.IsAny<Movie>(),It.IsAny<List<int>>(),It.IsAny<List<int>>()));
        }

        public static void MockUpdateMovie()
        {
            MovieRepoMock.Setup(repo => repo.UpdateMovieByMovieId(It.IsAny<int>(), It.IsAny<Movie>(), It.IsAny<List<int>>(), It.IsAny<List<int>>()));
        }

        public static void MockDeleteMovie()
        {
            MovieRepoMock.Setup(repo => repo.DeleteMovie(It.IsAny<int>()));
        }
    }
}
    

