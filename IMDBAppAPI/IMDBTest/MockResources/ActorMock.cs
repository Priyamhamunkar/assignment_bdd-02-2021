﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Moq;
using Newtonsoft.Json;
using SessionDemoApp.Models;
using SessionDemoApp.Repository;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;

namespace SessionDemo.Test.MockResources
{
    public class ActorMock
    {
         public static readonly Mock<IActorRepository> ActorRepoMock = new Mock<IActorRepository>();

        public static void MockGetAll()
        {
            var actors = JsonConvert.DeserializeObject<List<Actor>>(
                           File.ReadAllText(Environment.CurrentDirectory +
                                            "\\ResponseData\\ActorData.json"));

            ActorRepoMock.Setup(repo => repo.ListAllActors()).Returns(actors);
        }
        public static void MockGet()
        {
            var actors = JsonConvert.DeserializeObject<List<Actor>>(
                           File.ReadAllText(Environment.CurrentDirectory +
                                            "\\ResponseData\\ActorData.json"));


            ActorRepoMock.Setup(repo => repo.GetById(It.IsAny<int>())).Returns((int id) => actors.Single(m => m.Id == id));
        }
        public static void MockAddActor()
        {
            ActorRepoMock.Setup(repo => repo.Add(It.IsAny<Actor>()));
        }
        public static void MockUpdateActor()
        {
            ActorRepoMock.Setup(repo => repo.Update(It.IsAny<int>(),It.IsAny<Actor>()));
        }
        public static void MockDeleteActor()
        {
            ActorRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }
    }
}