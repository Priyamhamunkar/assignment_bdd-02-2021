﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Moq;
using Newtonsoft.Json;
using SessionDemoApp.Models;
using SessionDemoApp.Repository;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;

namespace SessionDemo.Test.MockResources
{
    public class GenreMock
    {
        public static readonly Mock<IGenreRepository> GenreRepoMock = new Mock<IGenreRepository>();

        public static void MockGetAll()
        {
            var genres = JsonConvert.DeserializeObject<List<Genre>>(
                           File.ReadAllText(Environment.CurrentDirectory +
                                            "\\ResponseData\\GenreData.json"));

            GenreRepoMock.Setup(repo => repo.ListAllGenres()).Returns(genres);
        }
        public static void MockGet()
        {
            var genres = JsonConvert.DeserializeObject<List<Genre>>(
                           File.ReadAllText(Environment.CurrentDirectory +
                                            "\\ResponseData\\GenreData.json"));


            GenreRepoMock.Setup(repo => repo.GetById(It.IsAny<int>())).Returns((int id) => genres.Single(m => m.Id == id));
        }
        public static void MockAddGenre()
        {
            GenreRepoMock.Setup(repo => repo.Add(It.IsAny<Genre>()));
        }
        public static void MockUpdateGenre()
        {
            GenreRepoMock.Setup(repo => repo.Update(It.IsAny<int>(), It.IsAny<Genre>()));
        }
        public static void MockDeleteGenre()
        {
            GenreRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }
    }
}