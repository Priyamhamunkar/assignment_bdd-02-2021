﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Moq;
using Newtonsoft.Json;
using SessionDemoApp.Models;
using SessionDemoApp.Repository;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;

namespace SessionDemo.Test.MockResources
{
    public class ProducerMock
    {
        public static readonly Mock<IProducerRepository> ProducerRepoMock = new Mock<IProducerRepository>();

        public static void MockGetAll()
        {
            var producers = JsonConvert.DeserializeObject<List<Producer>>(
                           File.ReadAllText(Environment.CurrentDirectory +
                                            "\\ResponseData\\ProducerData.json"));

            ProducerRepoMock.Setup(repo => repo.ListAllProducers()).Returns(producers);
        }
        public static void MockGet()
        {
            var producers = JsonConvert.DeserializeObject<List<Producer>>(
                           File.ReadAllText(Environment.CurrentDirectory +
                                            "\\ResponseData\\ProducerData.json"));

            ProducerRepoMock.Setup(repo => repo.GetById(It.IsAny<int>())).Returns((int id) => producers.Single(m => m.Id == id));
        }
        public static void MockAddProducer()
        {
            ProducerRepoMock.Setup(repo => repo.Add(It.IsAny<Producer>()));
        }
        public static void MockUpdateProducer()
        {
            ProducerRepoMock.Setup(repo => repo.Update(It.IsAny<int>(), It.IsAny<Producer>()));
        }
        public static void MockDeleteProducer()
        {
            ProducerRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }
    }
}