Feature:  Producer Controller

Scenario:  Get All Producers
Given I am a client
When I make GET Request '<RequestURL>' 
Then response code must be '<StatusCode>' 
And response data must look like '<ResponseData>' 
Examples: 
| RequestURL   | StatusCode | ResponseData                                                                                                                                                                |
| producer     | 200        | [{"id":1,"name":"Ravi","bio":"Producer1","dob":"1998-11-03T00:00:00","gender":"Male"},{"id":2,"name":"Arun","bio":"Producer2","dob":"1999-12-07T00:00:00","gender":"Male"}] |

Scenario: Get Producer by Id
Given I am a client
When I make GET Request '<RequestURL>'
Then response code must be '<StatusCode>'
And response data must look like '<ResponseData>'
Examples: 
| RequestURL | StatusCode | ResponseData                                                                         |
| producer/1 | 200        | {"id":1,"name":"Ravi","bio":"Producer1","dob":"1998-11-03T00:00:00","gender":"Male"} |

Scenario: Create New producer
Given I am a client
When I am making a post request to '<RequestURL>' with the following Data '<RequestData>'
Then response code must be '<StatusCode>'
And response data must look like '<ResponseData>'
Examples: 
| RequestURL | RequestData                                                              | StatusCode | ResponseData                 |
| producer   | {"Name": "Ravi", "Bio": "Producer1","DOB": "11/3/1998","Gender": "Male"} | 200        | {"message":"Producer Added"} |

Scenario: Update Producer by Id
Given I am a client
When I make PUT Request '<RequestURL>' with the following Data '<RequestData>'
Then response code must be '<StatusCode>'
And response data must look like '<ResponseData>'
Examples: 
| RequestURL | RequestData                                                              | StatusCode | ResponseData                   |
| producer/1 | {"Name": "Ravi", "Bio": "Producer1","DOB": "11/3/1998","Gender": "Male"} | 200        | {"message":"Producer Updated"} |

Scenario: Delete Producer by Id
Given I am a client
When I make Delete Request '<RequestURL>'
Then response code must be '<StatusCode>'
And response data must look like '<ResponseData>'
Examples: 
| RequestURL | StatusCode | ResponseData                   |
| producer/1 | 200        | {"message":"Producer Deleted"} |

