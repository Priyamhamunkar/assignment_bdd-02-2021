﻿Feature:  Actor Controller

Scenario:  Get All Actors
Given I am a client
When I make GET Request '<RequestURL>' 
Then response code must be '<StatusCode>' 
And response data must look like '<ResponseData>' 
Examples: 
| RequestURL | StatusCode | ResponseData                                                                                                                                                             |
| actor  | 200        | [{"id":1,"name":"Mike","bio":"Actor1","dob":"1999-10-06T00:00:00","gender":"Male"},{"id":2,"name":"Crist","bio":"Actor2","dob":"1999-12-07T00:00:00","gender":"Female"}] |

Scenario: Get Actor by Id
Given I am a client
When I make GET Request '<RequestURL>'
Then response code must be '<StatusCode>'
And response data must look like '<ResponseData>'
Examples: 
| RequestURL | StatusCode | ResponseData                                                                      |
| actor/1    | 200        | {"id":1,"name":"Mike","bio":"Actor1","dob":"1999-10-06T00:00:00","gender":"Male"} |

Scenario: Create New Actor
Given I am a client
When I am making a post request to '<RequestURL>' with the following Data '<RequestData>'
Then response code must be '<StatusCode>'
And response data must look like '<ResponseData>'
Examples: 
| RequestURL | RequestData                                                       | StatusCode | ResponseData              |
| actor      | {"Name":"Mike","Bio":"Actor1","DOB":"10-06-1999","Gender":"Male"} | 200        | {"message":"Actor Added"} |

Scenario: Update Actor by Id
Given I am a client
When I make PUT Request '<RequestURL>' with the following Data '<RequestData>'
Then response code must be '<StatusCode>'
And response data must look like '<ResponseData>'
Examples: 
| RequestURL | RequestData                                                       | StatusCode | ResponseData                |
| actor/1    | {"Name":"Mike","Bio":"Actor1","DOB":"10-06-1999","Gender":"Male"} | 200        | {"message":"Actor Updated"} |

Scenario: Delete Actor by Id
Given I am a client
When I make Delete Request '<RequestURL>'
Then response code must be '<StatusCode>'
And response data must look like '<ResponseData>'
Examples: 
| RequestURL | StatusCode | ResponseData    |
| actor/1    | 200        | {"message":"Actor Deleted"} |

