Feature:  Genre Controller

Scenario:  Get All Genres
Given I am a client
When I make GET Request '<RequestURL>' 
Then response code must be '<StatusCode>' 
And response data must look like '<ResponseData>' 
Examples: 
| RequestURL | StatusCode | ResponseData                                                                 |
| genre | 200        | [{"id":1,"name":"Humour"},{"id":2,"name":"Romantic"},{"id":3,"name":"Action"}] |

Scenario: Get Genre by Id
Given I am a client
When I make GET Request '<RequestURL>'
Then response code must be '<StatusCode>'
And response data must look like '<ResponseData>'
Examples: 
| RequestURL | StatusCode | ResponseData      |
| genre/1    | 200        | {"id":1,"name":"Humour"} |

Scenario: Create New Genre
Given I am a client
When I am making a post request to '<RequestURL>' with the following Data '<RequestData>'
Then response code must be '<StatusCode>'
And response data must look like '<ResponseData>'
Examples: 
| RequestURL | RequestData       | StatusCode | ResponseData              |
| genre      | {"name":"Humour"} | 200        | {"message":"Genre Added"} |

Scenario: Update Genre by Id
Given I am a client
When I make PUT Request '<RequestURL>' with the following Data '<RequestData>'
Then response code must be '<StatusCode>'
And response data must look like '<ResponseData>'
Examples: 
| RequestURL | RequestData       | StatusCode | ResponseData                |
| genre/1    | {"name":"Humour"} | 200        | {"message":"Genre Updated"} |

Scenario: Delete Actor by Id
Given I am a client
When I make Delete Request '<RequestURL>'
Then response code must be '<StatusCode>'
And response data must look like '<ResponseData>'
Examples: 
| RequestURL | StatusCode | ResponseData    |
| genre/1    | 200        | {"message":"Genre Deleted"} |

