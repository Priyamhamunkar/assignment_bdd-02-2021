Feature:  Movie Controller

Scenario: Get All Movies
Given I am a client
When I make GET Request '<RequestURL>' 
Then response code must be '<StatusCode>' 
And response data must look like '<ResponseData>' 
Examples: 
| RequestURL | StatusCode | ResponseData                                                                                                                                                                                                                                                                                                                              |
| movie      | 200        | [{"id":1,"name":"Movie1","yearOfRelease":"1999","plot":"FirstMovie","poster":"ABC","actorlist":["Mike","Crist"],"producerName":"Ravi","genreList":["Humour","Action"]},{"id":2,"name":"Movie2","yearOfRelease":"2000","plot":"SecondMovie","poster":"DEF","actorlist":["Crist"],"producerName":"Arun","genreList":["Romantic","Action"]}] |

Scenario: Get Movie by Id
Given I am a client
When I make GET Request '<RequestURL>'
Then response code must be '<StatusCode>'
And response data must look like '<ResponseData>'
Examples: 
| RequestURL | StatusCode | ResponseData                                                            |
| movie/1    | 200        | {"id":1,"name":"Movie1","yearOfRelease":"1999","plot":"FirstMovie","poster":"ABC","actorlist":["Mike","Crist"],"producerName":"Ravi","genreList":["Humour","Action"]}|

Scenario: Create New Movie
Given I am a client
When I am making a post request to '<RequestURL>' with the following Data '<RequestData>'
Then response code must be '<StatusCode>'
And response data must look like '<ResponseData>'
Examples: 
| RequestURL | RequestData                                                                                                                  | StatusCode | ResponseData |
| movie      | {"Name":"Movie1","Plot":"FirstMovie","YearOfRelease":"1999","Poster":"ABC","ActorIds":[1,2],"ProducerId":1,"GenreIds":[1,3]} | 200        | Movie Added  |

Scenario: Update Movie by Id
Given I am a client
When I make PUT Request '<RequestURL>' with the following Data '<RequestData>'
Then response code must be '<StatusCode>'
And response data must look like '<ResponseData>'
Examples: 
| RequestURL | RequestData                                                                                                               | StatusCode | ResponseData |
| movie/1    | {"Name":"Movie1","Plot":"MyMovie","YearOfRelease":"1999","Poster":"ABC","ActorIds":[1,2],"ProducerId":1,"GenreIds":[1,3]} | 200        | Movie Updated  |

Scenario: Delete Movie by Id
Given I am a client
When I make Delete Request '<RequestURL>'
Then response code must be '<StatusCode>'
And response data must look like '<ResponseData>'
Examples: 
| RequestURL | StatusCode | ResponseData    |
| movie/1    | 200        | {"message":"Movie Deleted"} |