﻿using SessionDemo.Test.MockResources;
using SessionDemoApp;
using SessionDemoApp.Services;
using System;
using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;

namespace SessionDemo.Test.StepFiles
{
    [Scope(Feature = "Movie Controller")]
    [Binding]
    
    public class MovieSteps : BaseSteps
    {
        

        public MovieSteps(CustomWebApplicationFactory<TestStartup> factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    // Producer Repo
                    //_context = context;
                     services.AddScoped(service => MovieMock.MovieRepoMock.Object);
                     services.AddScoped<IMovieService, MovieService>();
                    services.AddScoped(service => MovieMock.ActorRepoMock.Object);
                    services.AddScoped<IActorService, ActorService>();
                    services.AddScoped(service => MovieMock.ProducerRepoMock.Object);
                    services.AddScoped<IProducerService, ProducerService>();
                    services.AddScoped(service => MovieMock.GenreRepoMock.Object);
                    services.AddScoped<IGenreService, GenreService>();
                });
            }))
        {
        }

        [BeforeScenario]
        public static void Mocks()
        {
            MovieMock.MockGetAll();
            MovieMock.MockGetMovieById();
            MovieMock.MockAddMovie();
            MovieMock.MockUpdateMovie();
            MovieMock.MockDeleteMovie();
        }
    }
}
