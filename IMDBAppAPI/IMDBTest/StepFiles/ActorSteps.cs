﻿using SessionDemo.Test.MockResources;
using SessionDemoApp;
using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;
using SessionDemoApp.Services;

namespace SessionDemo.Test.StepFiles
{
    [Scope(Feature = "Actor Controller")]
    [Binding]
    public class ActorSteps : BaseSteps
    {
        public ActorSteps(CustomWebApplicationFactory<TestStartup> factory, ScenarioContext context)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    // Producer Repo
                     services.AddScoped(service => ActorMock.ActorRepoMock.Object);
                    services.AddScoped<IActorService, ActorService>();
                });
            }))
        {
        }

        [BeforeScenario]
        public static void Mocks()
        {
            ActorMock.MockGetAll();
            ActorMock.MockGet();
            ActorMock.MockAddActor();
            ActorMock.MockUpdateActor();
            ActorMock.MockDeleteActor();
        }
    }
}