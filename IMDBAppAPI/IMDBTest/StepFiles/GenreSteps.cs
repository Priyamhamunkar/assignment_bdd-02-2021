﻿using SessionDemo.Test.MockResources;
using SessionDemoApp;
using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;
using SessionDemoApp.Services;

namespace SessionDemo.Test.StepFiles
{
    [Scope(Feature = "Genre Controller")]
    [Binding]
    public class GenreSteps : BaseSteps
    {
        public GenreSteps(CustomWebApplicationFactory<TestStartup> factory, ScenarioContext context)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    // Producer Repo
                    services.AddScoped(service => GenreMock.GenreRepoMock.Object);
                    services.AddScoped<IGenreService, GenreService>();
                });
            }))
        {
        }

        [BeforeScenario]
        public static void Mocks()
        {
            GenreMock.MockGetAll();
            GenreMock.MockGet();
            GenreMock.MockAddGenre();
            GenreMock.MockUpdateGenre();
            GenreMock.MockDeleteGenre();
        }
    }
}