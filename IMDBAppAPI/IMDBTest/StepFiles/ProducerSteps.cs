﻿using SessionDemo.Test.MockResources;
using SessionDemoApp;
using TechTalk.SpecFlow;
using Microsoft.Extensions.DependencyInjection;
using SessionDemoApp.Services;

namespace SessionDemo.Test.StepFiles
{
    [Scope(Feature = "Producer Controller")]
    [Binding]
    public class ProducerSteps : BaseSteps
    {
        public ProducerSteps(CustomWebApplicationFactory<TestStartup> factory, ScenarioContext context)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    // Producer Repo
                    services.AddScoped(service => ProducerMock.ProducerRepoMock.Object);
                    services.AddScoped<IProducerService, ProducerService>();
                });
            }))
        {
        }

        [BeforeScenario]
        public static void Mocks()
        {
            ProducerMock.MockGetAll();
            ProducerMock.MockGet();
            ProducerMock.MockAddProducer();
            ProducerMock.MockUpdateProducer();
            ProducerMock.MockDeleteProducer();
        }
    }
}