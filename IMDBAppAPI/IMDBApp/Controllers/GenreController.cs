﻿using Microsoft.AspNetCore.Mvc;
using SessionDemoApp.Models;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;
using SessionDemoApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SessionDemoApp.Controllers
{
    [Route("[controller]")]
    public class GenreController : BaseController
    {
        private readonly IGenreService _genreService;

        public GenreController(IGenreService genreService)
        {
            _genreService = genreService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var genres = _genreService.ListAllGenres();
            return Ok(genres);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var genres= _genreService.GetById(id);
            return Ok(genres);
        }

        [HttpPost]
        public IActionResult Post([FromBody] GenreRequest genreRequest)
        {
            if (ModelState.IsValid)
            {
                _genreService.Add(genreRequest);
                return Ok(new { message = "Genre Added" });
            }
            return BadRequest(ModelState);
        }


        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] GenreRequest genreRequest)
        {
            if (id < 0)
            {
                return BadRequest("Wrong Id");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    _genreService.Update(id, genreRequest);
                    return Ok(new { message = "Genre Updated" });
                }
                else
                {
                    return BadRequest(ModelState);

                }
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (id < 0)
            {
                return BadRequest("Wrong Id");
            }
            else
            {
                _genreService.Delete(id);
                return Ok(new { message = "Genre Deleted" });
            }
        }

    }
}
