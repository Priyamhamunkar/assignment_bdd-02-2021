﻿using Firebase.Storage;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SessionDemoApp.Models;
using SessionDemoApp.Repository;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;
using SessionDemoApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SessionDemoApp.Controllers
{
    [Route("[controller]")]
    public class MovieController : BaseController
    {
        private readonly IMovieService _movieService;
    
        public MovieController(IMovieService movieService)
        {
            _movieService = movieService;
           
        }

        // POST api/<MovieController>
        [HttpPost]
        public IActionResult Post([FromBody] MovieRequest movieRequest)
        {
             _movieService.AddMovie(movieRequest);
            return Ok("Movie Added");
        }

        // GET: api/<MovieController>
        [HttpGet]
        public IActionResult Get()
        {
            var movie= _movieService.ListAllMovies();
            return Ok(movie);
        }

        
        // GET api/<MovieController>/5
        [HttpGet("{id}")]
        public IActionResult GetMovieById(int id)
        {
            var movie= _movieService.GetMovieById(id);
            return Ok(movie);
        }

 
        // PUT api/<MovieController>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] MovieRequest movieRequest)
        {
            if (id < 0)
            {
                return BadRequest("Wrong Id");
            }
            else
            {
                _movieService.UpdateMovieByMovieId(id, movieRequest);
                return Ok("Movie Updated");
            }
        }

        
        // DELETE api/<MovieController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (id < 0)
            {
                return BadRequest("Wrong Id");
            }
            else
            {
                _movieService.DeleteMovie(id);
                return Ok(new { message = "Movie Deleted" });
            }
        }


        [HttpPost("upload")]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return Content("file not selected");
            var task = await new FirebaseStorage("imdbapi-3461c.appspot.com")
                    .Child("Images")
                    .Child(Guid.NewGuid().ToString() + ".jpg")
                    .PutAsync(file.OpenReadStream());
            return Ok(task);
        }

    }
}
