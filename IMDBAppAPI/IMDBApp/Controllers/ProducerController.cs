﻿using Microsoft.AspNetCore.Mvc;
using SessionDemoApp.Models;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;
using SessionDemoApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SessionDemoApp.Controllers
{
    [Route("[controller]")]
    public class ProducerController : BaseController
    {
        private readonly IProducerService _producerService;

        public ProducerController(IProducerService producerService)
        {
            _producerService = producerService;
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var producer= _producerService.GetById(id);
            return Ok(producer);
        }

        [HttpGet]
        public IActionResult ListAllProducers()
        {
            var producers = _producerService.ListAllProducers();
            return Ok(producers);
        }

        [HttpPost]
        public IActionResult Post([FromBody] ProducerRequest producerRequest)
        {
            if (ModelState.IsValid)
            {
                _producerService.Add(producerRequest);
                return Ok(new { message = "Producer Added" });
            }
            return BadRequest(ModelState);
        }


        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ProducerRequest producerRequest)
        {
            if (id < 0)
            {
                return BadRequest("Wrong Id");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    _producerService.Update(id, producerRequest);
                    return Ok(new { message = "Producer Updated" });
                }
                else
                {
                    return BadRequest(ModelState);

                }
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (id < 0)
            {
                return BadRequest("Wrong Id");
            }
            else
            {
                _producerService.Delete(id);
                return Ok(new { message = "Producer Deleted" });
            }
        }

    }
}
