﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SessionDemoApp.Controllers
{
    [Route("api/movie/{movieId}/[controller]")]
    [ApiController]
    public class ReviewController : ControllerBase
    {
        [HttpGet]
        //[Route("{movieId}")]
        public IActionResult Get(int movieId)
        {
            return Ok(movieId);
        }
    }
}

