﻿using Microsoft.AspNetCore.Mvc;
using SessionDemoApp.Controllers;
using SessionDemoApp.Models;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;
using SessionDemoApp.Services;
using System.Collections.Generic;

namespace SessionDemoApp
{
    [Route("[controller]")]
    public class ActorController : BaseController 
    {
        private readonly IActorService _actorService;

        public ActorController(IActorService actorService)
        {
            _actorService = actorService;
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
             var actor= _actorService.GetById(id);
            return Ok(actor);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var actors = _actorService.ListAllActors();
            return Ok(actors);
        }
       
        [HttpPost]
        public IActionResult Post([FromBody] ActorRequest actorRequest)
        {
            if (ModelState.IsValid)
            {
                _actorService.Add(actorRequest);
                return Ok(new { message = "Actor Added" });
            }
            return BadRequest(ModelState);
        }

        
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ActorRequest actorRequest)
        {
                if (ModelState.IsValid)
                {
                    _actorService.Update(id, actorRequest);
                    return Ok(new { message = "Actor Updated" });
                }
                else
                {
                    return BadRequest(ModelState);
                }
        }
        

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (id < 0)
            {
                return BadRequest("Wrong Id");
            }
            else
            {
                _actorService.Delete(id);
                return Ok(new { message = "Actor Deleted" });
            }
        }
        
    }
}
