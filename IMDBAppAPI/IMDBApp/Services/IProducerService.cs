﻿using SessionDemoApp.Models;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SessionDemoApp.Services
{
    public interface IProducerService
    {
        public IEnumerable<ProducerResponse> ListAllProducers();
        public ProducerResponse GetById(int id);
        public void Add(ProducerRequest producerRequest);
        public void Update(int id, ProducerRequest producerRequest);
        public void Delete(int id);
    }
}
