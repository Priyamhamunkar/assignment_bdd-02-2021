﻿using Microsoft.AspNetCore.Mvc;
using SessionDemoApp.Models;
using SessionDemoApp.Repository;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SessionDemoApp.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IActorRepository _actorRepository;
        private readonly IGenreRepository _genreRepository;
        private readonly IProducerRepository _producerRepository;

        public MovieService(IMovieRepository movieRepository, IActorRepository actorRepository, IGenreRepository genreRepository, 
            IProducerRepository producerRepository)
        {
            _movieRepository = movieRepository;
            _actorRepository = actorRepository;
            _genreRepository = genreRepository;
            _producerRepository = producerRepository;
        }

        public void AddMovie(MovieRequest movieRequest)
        {
            Movie move = new Movie
            {
                Name = movieRequest.Name,
                Plot = movieRequest.Plot,
                Poster = movieRequest.Poster,
                ProducerId = movieRequest.ProducerId,
                YearOfRelease = movieRequest.YearOfRelease
            };
            var actorIds = movieRequest.ActorIds;
            var genreIds = movieRequest.GenreIds;
            _movieRepository.AddMovie(move, actorIds, genreIds);
        }

        public IEnumerable<MovieResponse> ListAllMovies()
        {
            List<MovieResponse> movieResponses = new List<MovieResponse>();
            List<Actor> actorlist = new List<Actor>();
            List<Genre> genrelist = new List<Genre>();
            Producer producer=new Producer();
            var movieData = _movieRepository.ListAllMovies();
            foreach (var movie in movieData)
            {
                actorlist = _actorRepository.GetActorsByMovieId(movie.Id).ToList();
                genrelist = _genreRepository.GetGenresByMovieId(movie.Id).ToList();
                producer = _producerRepository.GetById(movie.ProducerId);
                var movieResponse = new MovieResponse()
                {
                    Actorlist = actorlist.Select(a => a.Name).ToList(),
                    Id = movie.Id,
                    Name = movie.Name,
                    Plot = movie.Plot,
                    Poster = movie.Poster,
                    GenreList = genrelist.Select(a => a.Name).ToList(),
                    ProducerName = producer.Name,
                    YearOfRelease = movie.YearOfRelease
                };
                movieResponses.Add(movieResponse);
                actorlist = null;
                genrelist = null;
            }
            return movieResponses;
        }

        public MovieResponse GetMovieById(int id)
        {
            List<Actor> actorlist = new List<Actor>();
            List<Genre> genrelist = new List<Genre>();
            Producer producer = new Producer();
            var movieData = _movieRepository.GetMovieById(id);
            actorlist = _actorRepository.GetActorsByMovieId(id).ToList();
            genrelist = _genreRepository.GetGenresByMovieId(id).ToList();
            producer = _producerRepository.GetById(movieData.ProducerId);
            var movieResponse = new MovieResponse()
                {
                    Actorlist = actorlist.Select(a => a.Name).ToList(),
                    Id = movieData.Id,
                    Name = movieData.Name,
                    Plot = movieData.Plot,
                    Poster = movieData.Poster,
                    GenreList = genrelist.Select(genre => genre.Name).ToList(),
                    ProducerName = producer.Name,
                    YearOfRelease = movieData.YearOfRelease
                };
            return movieResponse;
        }

        
        public void UpdateMovieByMovieId(int id,MovieRequest movieRequest)
        {
            Movie move = new Movie
            {
                Name = movieRequest.Name,
                Plot = movieRequest.Plot,
                Poster = movieRequest.Poster,
                ProducerId = movieRequest.ProducerId,
                YearOfRelease = movieRequest.YearOfRelease
            };
            var actorIds = movieRequest.ActorIds;
            var genreIds = movieRequest.GenreIds;
            _movieRepository.UpdateMovieByMovieId(id, move,actorIds,genreIds);
        }
        
        public void DeleteMovie(int id)
        {
            _movieRepository.DeleteMovie(id);
        }
    }
}
