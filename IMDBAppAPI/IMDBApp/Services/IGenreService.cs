﻿using SessionDemoApp.Models;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SessionDemoApp.Services
{
    public interface IGenreService
    {
        public IEnumerable<GenreResponse> ListAllGenres();
        public GenreResponse GetById(int id);
        public void Add(GenreRequest genreRequest);
        public void Update(int id, GenreRequest genreRequest);
        public void Delete(int id);
    }
}
