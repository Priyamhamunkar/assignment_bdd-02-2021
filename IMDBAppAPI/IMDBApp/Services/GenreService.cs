﻿using SessionDemoApp.Models;
using SessionDemoApp.Repository;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SessionDemoApp.Services
{
    public class GenreService :IGenreService
    {
        private readonly IGenreRepository _genreRepository;

        public GenreService(IGenreRepository genreRepository)
        {
            _genreRepository = genreRepository;
        }
        public GenreResponse GetById(int id)
        {
            var response = _genreRepository.GetById(id);
            var genre = new GenreResponse()
            {
                Id = response.Id,
                Name = response.Name
            };
            return genre;
        }
        public IEnumerable<GenreResponse> ListAllGenres()
        {
            List<GenreResponse> genreResponses = new List<GenreResponse>();
            var response = _genreRepository.ListAllGenres();
            foreach (var r in response)
            {
                var genre = new GenreResponse()
                {
                    Id = r.Id,
                    Name = r.Name
                };
                genreResponses.Add(genre);
            }
            return genreResponses;
        }
        public void Add(GenreRequest genreRequest)
        {
            Genre genre=new Genre()
            {
                Name = genreRequest.Name
            };
            _genreRepository.Add(genre);
        }
        public void Update(int id, GenreRequest genreRequest)
        {
            Genre genre = new Genre()
            {
                Name = genreRequest.Name
            };
            _genreRepository.Update(id, genre);
        }
        public void Delete(int id)
        {
            _genreRepository.Delete(id);
        }
    }
}
