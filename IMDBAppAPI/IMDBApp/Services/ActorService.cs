﻿using SessionDemoApp.Models;
using System.Collections.Generic;
using SessionDemoApp.Repository;
using Microsoft.AspNetCore.Mvc;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;


namespace SessionDemoApp.Services
{
    public class ActorService : IActorService
    {
        private readonly IActorRepository _actorRepository;

        public ActorService(IActorRepository actorRepository)
        {
            _actorRepository = actorRepository;
        }

        public IEnumerable<ActorResponse> ListAllActors()
        {
            List<ActorResponse> actorResponses=new List<ActorResponse>();
            var response= _actorRepository.ListAllActors();
              foreach(var r in response)
            {
                var actor = new ActorResponse()
                {
                    Id = r.Id,
                    Name=r.Name,
                    Bio=r.Bio,
                    Gender=r.Gender,
                    DOB=r.DOB
                };
                actorResponses.Add(actor);
            }
            return actorResponses;
        }
        public ActorResponse GetById(int id)
        {
            var response = _actorRepository.GetById(id);
                var actorResponse = new ActorResponse()
                {
                    Id = response.Id,
                    Name = response.Name,
                    Bio = response.Bio,
                    Gender = response.Gender,
                    DOB = response.DOB
                };
            return actorResponse;
        }

        public void Add(ActorRequest actorRequest)
        {
            Actor actor = new Actor
            {
                Name = actorRequest.Name,
                Bio = actorRequest.Bio,
                Gender = actorRequest.Gender,
                DOB = actorRequest.DOB
            };
            _actorRepository.Add(actor);
        }
        public void Update(int id, ActorRequest actorRequest)
        {
            if(id<0)
            {
                throw new System.Exception("wrong id");
            }
            else
            {
                Actor actor = new Actor
                {
                    Name = actorRequest.Name,
                    Bio = actorRequest.Bio,
                    Gender = actorRequest.Gender,
                    DOB = actorRequest.DOB
                };
                _actorRepository.Update(id, actor);
            }
        }
        public void Delete(int id)
        {
            _actorRepository.Delete(id);
        }
    }
}
