﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using SessionDemoApp.Models;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;

namespace SessionDemoApp.Services
{
    public interface IActorService
    {
        public IEnumerable<ActorResponse> ListAllActors();
        public ActorResponse GetById(int id);
        public void Add(ActorRequest actorRequest);
        public void Update(int id, ActorRequest actorRequest);
        public void Delete(int id);
       
    }
}
