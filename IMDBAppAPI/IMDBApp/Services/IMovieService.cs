﻿using Microsoft.AspNetCore.Mvc;
using SessionDemoApp.Models;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SessionDemoApp.Services
{
    public interface IMovieService
    {
        public IEnumerable<MovieResponse> ListAllMovies();
        public MovieResponse GetMovieById(int id);
        public void AddMovie(MovieRequest movieRequest);
        public void UpdateMovieByMovieId(int id,MovieRequest movieRequest);
        public void DeleteMovie(int id);

    }
}
