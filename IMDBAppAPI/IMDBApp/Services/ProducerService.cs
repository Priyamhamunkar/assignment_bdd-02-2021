﻿using SessionDemoApp.Models;
using SessionDemoApp.Repository;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SessionDemoApp.Services
{
    public class ProducerService : IProducerService
    {
        private readonly IProducerRepository _producerRepository;

        public ProducerService(IProducerRepository producerRepository)
        {
            _producerRepository = producerRepository;
        }
        public ProducerResponse GetById(int id)
        {
            var response = _producerRepository.GetById(id);
            var producerResponse = new ProducerResponse
            {
                Id = response.Id,
                Name = response.Name,
                Bio = response.Bio,
                Gender = response.Gender,
                DOB = response.DOB
            };
            return producerResponse;
        }
        public IEnumerable<ProducerResponse> ListAllProducers()
        {
            List<ProducerResponse> producerResponses = new List<ProducerResponse>();
            var response = _producerRepository.ListAllProducers();
            foreach (var r in response)
            {
                var producerResponse = new ProducerResponse
                {
                    Id = r.Id,
                    Name = r.Name,
                    Bio = r.Bio,
                    Gender = r.Gender,
                    DOB = r.DOB
                };
                producerResponses.Add(producerResponse);
            }
            return producerResponses;
        }
        public void Add(ProducerRequest producerRequest)
        {
            Producer producer=new Producer
            {
                Name = producerRequest.Name,
                Bio = producerRequest.Bio,
                Gender = producerRequest.Gender,
                DOB = producerRequest.DOB
            };
            _producerRepository.Add(producer);
        }
        public void Update(int id, ProducerRequest producerRequest)
        {
            Producer producer = new Producer
            {
                Name = producerRequest.Name,
                Bio = producerRequest.Bio,
                Gender = producerRequest.Gender,
                DOB = producerRequest.DOB
            };
            _producerRepository.Update(id, producer);
        }
        public void Delete(int id)
        {
            _producerRepository.Delete(id);
        }
    }
}
