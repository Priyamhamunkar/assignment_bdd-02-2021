﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SessionDemoApp.RequestModel
{
    public class GenreRequest
    {
        public string Name { get; set; }
    }
}
