﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SessionDemoApp.RequestModel
{
    public class MovieRequest
    {
       // public int Id { get; set; }
        public string Name { get; set; }
        public string YearOfRelease { get; set; }
        public string Plot { get; set; }
        public List<int> ActorIds { get; set; }
        public List<int> GenreIds { get; set; }
        public int ProducerId { get; set; }
        public string Poster { get; set; }
    }
}
