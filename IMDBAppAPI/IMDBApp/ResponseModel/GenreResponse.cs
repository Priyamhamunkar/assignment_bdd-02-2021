﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SessionDemoApp.ResponseModel
{
    public class GenreResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
