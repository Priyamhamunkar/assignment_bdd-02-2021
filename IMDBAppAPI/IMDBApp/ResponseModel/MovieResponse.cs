﻿using SessionDemoApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SessionDemoApp.ResponseModel
{
    public class MovieResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string YearOfRelease { get; set; }
        public string Plot { get; set; }
        public string Poster { get; set; }
        public List<string> Actorlist { get; set; }
        public String ProducerName { get; set; }
        public List<string> GenreList { get; set; }
    }
}
