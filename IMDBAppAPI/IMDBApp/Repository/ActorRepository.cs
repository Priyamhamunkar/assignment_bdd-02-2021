﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SessionDemoApp.Models;
using System.Configuration;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;

namespace SessionDemoApp.Repository
{
    public class ActorRepository :  GenericRepository, IActorRepository
    {
        public ActorRepository(IOptions<ConnectionString> connectionString) :
            base(connectionString)
        {
          
        }
        
        public IEnumerable<Actor> ListAllActors()
        {
            const string query = @"
SELECT *
FROM Actors";
            using (SqlConnection connection = new SqlConnection(_connectionString.IMDB))
            {
                var actors = connection.Query<Actor>(query);
                return actors;
            }
        }

        public Actor GetById(int id)
        {
            const string query = @"
SELECT *
FROM Actors 
WHERE Id=@id";
            using (var connection = new SqlConnection(_connectionString.IMDB))
            {
                var actor = connection.QuerySingle<Actor>(query, new
                {
                    Id = id
                });
                return actor;
            }
        }

        public IEnumerable<Actor> GetActorsByMovieId(int id)
        {
            const string query = @"
SELECT A.Name
FROM Actors A
INNER JOIN MovieActors MA ON MA.ActorId=A.Id
WHERE MA.MovieId=@Id";
            using (var connection = new SqlConnection(_connectionString.IMDB))
            {
                var actors = connection.Query<Actor>(query, new
                {
                    Id = id
                });
                return actors;
            }
        }

        public void Add(Actor actor)
        {
            using (var connection = new SqlConnection(_connectionString.IMDB))
            {
                var values = new
                {
                    actor.Name,
                    actor.Bio,
                    actor.DOB,
                    actor.Gender
                };
                connection.Execute("AddNewActorDetails", values, commandType: CommandType.StoredProcedure);
            }
        }
        
        public void Update(int id, Actor actor)
        {
            using (var connection = new SqlConnection(_connectionString.IMDB))
            {
                var values = new
                {
                    Id = id,
                    actor.Name,
                    actor.Bio,
                    actor.DOB,
                    actor.Gender
                };
                    connection.Execute("UpdateActorDetails", values, commandType: CommandType.StoredProcedure);
            }
        }

        public void Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString.IMDB))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@Id", id);
                connection.Execute("DeleteActorById", param, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
