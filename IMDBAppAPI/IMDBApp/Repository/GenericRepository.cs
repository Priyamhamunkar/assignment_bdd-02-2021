﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SessionDemoApp.Repository
{
    public class GenericRepository
    {
        public readonly ConnectionString _connectionString;
        public GenericRepository(IOptions<ConnectionString> connectionString)
        {
            _connectionString = connectionString.Value;
        }
    }
}
