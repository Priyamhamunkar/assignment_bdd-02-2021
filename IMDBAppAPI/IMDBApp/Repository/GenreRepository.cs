﻿using Dapper;
using Microsoft.Extensions.Options;
using SessionDemoApp.Models;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SessionDemoApp.Repository
{
    public class GenreRepository: GenericRepository,IGenreRepository
    {
        public GenreRepository(IOptions<ConnectionString> connectionString) :
           base(connectionString)
        {

        }

        public Genre GetById(int id)
        {
            const string query = @"
SELECT *
FROM Genre 
WHERE Id=@id";
            using (var connection = new SqlConnection(_connectionString.IMDB))
            {
                var genre = connection.QuerySingle<Genre>(query, new
                {
                    Id = id
                });
                return genre;
            }
        }
        public IEnumerable<Genre> ListAllGenres()
        {
            const string query = @"
SELECT *
FROM Genre";
            using (var connection = new SqlConnection(_connectionString.IMDB))
            {
                var genre = connection.Query<Genre>(query);
                return genre;
            }
        }

        public IEnumerable<Genre> GetGenresByMovieId(int id)
        {
            const string query = @"
SELECT G.Name
FROM Genre G
INNER JOIN MovieGenres MG ON MG.GenreId=G.Id
WHERE MG.MovieId=@Id";
            using (var connection = new SqlConnection(_connectionString.IMDB))
            {
                var genres = connection.Query<Genre>(query, new
                {
                    Id = id
                });
                return genres;
            }
        }

        public void Add(Genre genre)
        {
            using (var connection = new SqlConnection(_connectionString.IMDB))
            {
                var values = new
                {
                    genre.Name
                };
                connection.Execute("AddNewGenreDetails", values, commandType: CommandType.StoredProcedure);
            }
        }

        public void Update(int id, Genre genre)
        {
            using (var connection = new SqlConnection(_connectionString.IMDB))
            {
                var values = new 
                { 
                    Id=id,
                    genre.Name
                };
                connection.Execute("UpdateGenreDetails", values, commandType: CommandType.StoredProcedure);
            }
        }

        public void Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString.IMDB))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@Id", id);
                connection.Execute("DeleteGenreById", param, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
