﻿using SessionDemoApp.Models;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SessionDemoApp.Repository
{
    public interface IProducerRepository
    {
        public IEnumerable<Producer> ListAllProducers();
        public Producer GetById(int id);
        public void Add(Producer producer);
        public void Update(int id, Producer producer);
        public void Delete(int id);
    }
}
