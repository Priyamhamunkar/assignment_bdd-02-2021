﻿using Microsoft.AspNetCore.Mvc;
using SessionDemoApp.Models;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SessionDemoApp.Repository
{
    public interface IMovieRepository
    {
        public void AddMovie(Movie movie, List<int> actorIds,List<int> genreIds);
        public IEnumerable<Movie> ListAllMovies();
        public Movie GetMovieById(int id);
        public void UpdateMovieByMovieId(int id,Movie movie, List<int> actorIds, List<int> genreIds);
        public void DeleteMovie(int id);
         
    }
}
