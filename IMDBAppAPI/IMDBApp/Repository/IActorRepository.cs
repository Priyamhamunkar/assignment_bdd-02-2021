﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using SessionDemoApp.Models;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;

namespace SessionDemoApp.Repository
{
    public interface IActorRepository
    {
        public IEnumerable<Actor> ListAllActors();
        public Actor GetById(int id);
        public IEnumerable<Actor> GetActorsByMovieId(int id);
        public void Add(Actor actor);
        public void Update(int id, Actor actor);
        public void Delete(int id);

    }
}
