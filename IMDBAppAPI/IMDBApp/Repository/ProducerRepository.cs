﻿using Dapper;
using Microsoft.Extensions.Options;
using SessionDemoApp.Models;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SessionDemoApp.Repository
{
    public class ProducerRepository:GenericRepository, IProducerRepository
    {
        public ProducerRepository(IOptions<ConnectionString> connectionString) :
          base(connectionString)
        {

        }

        public IEnumerable<Producer> ListAllProducers()
        {
            const string query = @"
SELECT *
FROM Producers";
            using (var connection = new SqlConnection(_connectionString.IMDB))
            {
                var producer = connection.Query<Producer>(query);
                return producer;
            }
        }

        public Producer GetById(int id)
        {
            const string query = @"
SELECT *
FROM producers 
WHERE Id=@id";
            using (var connection = new SqlConnection(_connectionString.IMDB))
            {
                var producer = connection.QuerySingle<Producer>(query, new
                {
                    Id = id
                });
                return producer;
            }
        }
        public void Add(Producer producer)
        {
            using (var connection = new SqlConnection(_connectionString.IMDB))
            {
                var values = new
                {
                    producer.Name,
                    producer.Bio,
                    producer.DOB,
                    producer.Gender
                };
                connection.Execute("AddNewProducerDetails", values, commandType: CommandType.StoredProcedure);
            }
        }

        public void Update(int id, Producer producer)
        {
            using (var connection = new SqlConnection(_connectionString.IMDB))
            {
                var values = new 
                {
                    Id=id,
                    producer.Name,
                    producer.Bio,
                    producer.DOB,
                    producer.Gender
                };
                connection.Execute("UpdateProducerDetails", values, commandType: CommandType.StoredProcedure);
            }
        }

        public void Delete(int id)
        {
            using (var connection = new SqlConnection(_connectionString.IMDB))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@Id", id);
                connection.Execute("DeleteProducerById", param, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
