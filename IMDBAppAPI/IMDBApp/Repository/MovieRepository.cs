﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SessionDemoApp.Models;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SessionDemoApp.Repository
{
    public class MovieRepository : GenericRepository, IMovieRepository
    {
        public MovieRepository(IOptions<ConnectionString> connectionString) :
          base(connectionString)
        {

        }

        public void AddMovie(Movie movie,List<int>actorIds, List<int>genreIds)
        {  
            using (var connection = new SqlConnection(_connectionString.IMDB))
            {
                var values = new 
                {
                    movie.Name,
                    movie.YearOfRelease,
                    movie.Poster,
                    movie.Plot,
                    movie.ProducerId,
                    ActorIds=string.Join(',',actorIds),
                    GenreIds=string.Join(',',genreIds)
                };
                connection.Query("AddNewMovieDetails", values, commandType: CommandType.StoredProcedure);
            }
        }
        
        public IEnumerable<Movie> ListAllMovies()
        {
            const string query = @"
SELECT M.Id AS [Id]
	,M.Name AS [Name]
	,M.YearOfRelease AS [YearOfRelease]
	,M.Plot AS [Plot]
	,M.Poster AS [Poster]
	,P.Id AS [ProducerId]
FROM Movies M
INNER JOIN Producers P ON P.Id= M.ProducerId";
            using (var connection = new SqlConnection(_connectionString.IMDB))
            {
                var movies = connection.Query<Movie>(query);
                return movies;
            }
        }

        
    public Movie GetMovieById(int id)
    {
        const string query = @"
SELECT  M.Id AS [Id]
    ,M.Name AS [Name]
    ,P.Id AS [ProducerId]
    ,M.Plot AS [Plot]
    ,M.YearOfRelease AS [YearOfRelease]
    ,M.Poster AS [Poster]
    ,M.Id AS [MovieId]       
FROM Movies M 
INNER JOIN Producers P ON M.ProducerId=P.Id
WHERE M.Id=@Id";
        using (var connection = new SqlConnection(_connectionString.IMDB))
        {
            var movie = connection.QuerySingle<Movie>(query, new
            {
                Id = id
            });
            return movie;
        }
    }
        
    public void UpdateMovieByMovieId(int id,Movie movie, List<int> actorIds, List<int> genreIds)
    {
            using (var connection = new SqlConnection(_connectionString.IMDB))
            {
                var values = new
                {
                    Id=id,
                    movie.Name,
                    movie.YearOfRelease,
                    movie.Poster,
                    movie.Plot,
                    movie.ProducerId,
                    ActorIds = string.Join(',', actorIds),
                    GenreIds = string.Join(',', genreIds)
                };
                connection.Query("UpdateMovieDetails", values, commandType: CommandType.StoredProcedure);
            }
        }
        
    public void DeleteMovie(int id)
    {
        using (var connection = new SqlConnection(_connectionString.IMDB))
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@MovieId", id);
            connection.Execute("DeleteMovieDetails", param, commandType: CommandType.StoredProcedure);
        }
    }

    }
}
