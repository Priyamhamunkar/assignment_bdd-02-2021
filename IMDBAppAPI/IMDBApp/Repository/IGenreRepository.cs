﻿using SessionDemoApp.Models;
using SessionDemoApp.RequestModel;
using SessionDemoApp.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SessionDemoApp.Repository
{
    public interface IGenreRepository
    {
        public IEnumerable<Genre> ListAllGenres();
        public Genre GetById(int id);
        public IEnumerable<Genre> GetGenresByMovieId(int id);
        public void Add(Genre genre);
        public void Update(int id, Genre genre);
        public void Delete(int id);
    }
}
